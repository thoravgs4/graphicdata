const canvas = document.querySelector('canvas')

const c = canvas.getContext('2d')
canvas.width = innerWidth
canvas.height = innerHeight

class Player {
    constructor (x,y, radius, color ){
        this.x = x
        this.y = y
        this.radius = radius
        this.color = color

    }
    draw(){
        c.beginPath()
        c.arc(this.x,this.y,this.radius,0, Math.PI*2, false)
        c.fillStyle = 'blue'
        c.fill()
    }
}

class Projectile {
    constructor (x,y, radius,velocity , color ){
        this.x = x
        this.y = y
        this.radius = radius
        this.velocity = velocity
        this.color = color

    }
    
    draw(){
        c.beginPath()
        c.arc(this.x,this.y,this.radius,0, Math.PI*2, false)
        c.fillStyle = 'red'
        c.fill()
    }
    update(){
        this.draw()
        this.x = this.x + this.velocity.x
        this.y = this.y + this.velocity.y}
}



//Enemies 

class Enemy {
    constructor (x,y, radius,velocity , color ){
        this.x = x
        this.y = y
        this.radius = radius
        this.velocity = velocity
        this.color = color

    }
    
    draw(){
        c.beginPath()
        c.arc(this.x,this.y,this.radius,0, Math.PI*2, false)
        c.fillStyle = 'green'
        c.fill()
    }
    update(){
        this.draw()
        this.x = this.x + this.velocity.x
        this.y = this.y + this.velocity.y}
}



const x_1 = canvas.width/2
const y_1 = canvas.height/2
const color = 'green'
const player = new Player (x_1, y_1, 12,color)

//Projctile 
const projectile = new Projectile(canvas.width/2,canvas.height/2,5,{x:1,y:1},'red')
const projectiles = []
const enemies =[]
function Emenies (){
    setInterval( ()=>{
         const radius = Math.random()  * (30-4) + 4
         let x
         let y 
         if ( Math.random() < 0.5){
 
              x = Math.random()  < 0.5 ? 0-radius : canvas.width + radius
              y = Math.random()*canvas.height
             }else{
                 x = Math.random() * canvas.width
                 y =  Math.random()  < 0.5 ? 0-radius : canvas.height + radius
             }
         
         const color = 'green'
         const angel = Math.atan2(canvas.height/2-y, canvas.width/2-x)
         const velocity ={
             x: Math.cos(angel),
             y:Math.sin(angel)
         }
         enemies.push(new Enemy(x,y,radius,velocity,color))
     }, 1000)
 }
 

let animateIndex 

//Animate function 
function animate (){
   animateIndex=  requestAnimationFrame(animate)
    c.clearRect(0,0, canvas.width,canvas.height)
    player.draw()
    projectiles.forEach((projectile)=> {
        projectile.update()
})
enemies.forEach( (Enemy, index)=>{
        Enemy.update()
        const dist = Math.hypot(player.x-Enemy.x,player.y-Enemy.y)
        if(dist -Enemy.radius-player.radius < 1){
            cancelAnimationFrame(animateIndex)

        }
        projectiles.forEach((projectile, projectileIndex)=>{
            const dist = Math.hypot(projectile.x-Enemy.x,projectile.y-Enemy.y)
            if(dist -Enemy.radius-projectile.radius < 1){
                setTimeout(( )=>{
                    enemies.splice(index,1)
                    projectiles.splice(projectileIndex,1)
                },0)}}) })
}


addEventListener('click', (event)=>{
    const angel = Math.atan2(
        event.clientY-canvas.height/2, 
        event.clientX-canvas.width/2
    )
    const velocity ={
        x: Math.cos(angel),
        y:Math.sin(angel)
    }

    projectiles.push(new  Projectile(canvas.width/2,canvas.height/2,5,
    velocity,
        'red'))

})

animate()
Emenies()


